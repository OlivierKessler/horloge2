    <body class="<?php echo $controller; ?>">
    <script src="Libraries/react-0.13.2/build/react.js"></script>
    <script src="Libraries/react-0.13.2/build/JSXTransformer.js"></script>
    <script src="Libraries/jquery/jquery-1.11.3.min.js"></script>
    <script src="Libraries/materialize/js/materialize.js"></script>
    <script src="Libraries/slidr-master/slidr-master/slidr.min.js"></script>
   <!-- <script src="Libraries/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>-->

    <?php
        if(!is_null($template_datas['error_code']) && $template_datas['error_code'] != ""){
            echo '<div class="warning">Login failed</div>';
        }

        echo $this->_hooksResult['HookBody'];
    ?>
