<html>
    <head>
        <link type="text/css" rel="stylesheet" href="Libraries/materialize/css/materialize.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="Themes/default/css/global.css" >
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    </head>

    <header>
        <?php
        echo $this->hooksResult['HookHeader'];
        ?>
    </header>
