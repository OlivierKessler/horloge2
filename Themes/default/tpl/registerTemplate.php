<div class="container">
    <div id="register-form"></div>
</div>


<script type="text/jsx">

    var Register = React.createClass({
        getInitialState: function() {
            return {
                submitted: null
            }
        }

        , render: function() {
            var submitted
            if (this.state.submitted !== null) {
                submitted = <div className="alert alert-success">
                    <p>LoginForm data:</p>
                    <pre><code>{JSON.stringify(this.state.submitted, null, '  ')}</code></pre>
                </div>
            }

            return <div>
                <div className="panel panel-default">
                    <div className="panel-body">
                        <h3 className="panel-title pull-left">Register</h3>
                        <RegisterForm ref="registerForm"  onSubmit={this.handleSubmit}/>
                    </div>
                    <ReturnLoginLink ref="extraLinks"/>
                </div>
                {submitted}
            </div>
        }

        , handleSubmit: function() {
            if (this.refs.registerForm.isValid()) {
                return true
            }
            this.setState({submitted: true})
            return false
        }
    })

    var ReturnLoginLink = React.createClass({
        render: function() {
            return <div className="extra-links">
                    {this.renderLoginLink()}
            </div>
        },

        renderLoginLink : function () {
            return <a href="index.php?controller=login&action=index">Return to login</a>
        }
    });



    /**
     * A register form
     */
    var RegisterForm = React.createClass({
        getDefaultProps: function() {
            return {
            }
        }

        , getInitialState: function() {
            return {errors: {}}
        }

        , isValid: function() {
            var fields = ['username', 'password']

            var errors = {}
            fields.forEach(function(field) {
                var value = trim(this.refs[field].getDOMNode().value)
                if (!value) {
                    errors[field] = 'This field is required'
                }
            }.bind(this))
            this.setState({errors: errors})

            var isValid = true
            for (var error in errors) {
                isValid = false
                break
            }
            return isValid
        }

        , getFormData: function() {
            var data = {
                username: this.refs.username.getDOMNode().value
                , password: this.refs.password.getDOMNode().value
            }

            return data
        }

        , handleChange: function(field, e) {
            var nextState = {}
            nextState[field] = e.target.checked
            this.setState(nextState)
        }

        , render: function() {
            return <form className="form-horizontal"
                action="index.php?controller=register&action=register" method="POST" onsubmit={this.props.onSubmit}>
                        {this.renderTextInput('username', 'User Name')}
                        {this.renderPasswordInput('password', 'Password')}
                        {this.renderSubmitInput('submit-register-form')}
            </form>
        }

        , renderTextInput: function(id, label) {
            return this.renderField(id, label,
                <input type="text" name="username" className="form-control validate" id={id} ref={id}/>
            )
        }

        , renderPasswordInput: function(id, label) {
            return this.renderField(id, label,
                <input type="password" name="password" className="form-control validate" id={id} ref={id} onChange={this.handleChange}/>
            )
        }
        , renderSubmitInput: function(id) {
            return   <input type="submit" id={id} className="btn btn-primary btn-block"/>

        }

        , renderField: function(id, label, field) {
            return <div className={$c('form-group', {'has-error': id in this.state.errors})}>
                <label htmlFor={id} className="col-sm-4 control-label">{label}</label>
                <div className="col-sm-6">
                    {field}
                </div>
            </div>
        }
    })

    React.render(<Register company="Olivier Kessler"/>, document.getElementById('register-form'))

    // Utils

    var trim = function() {
        var TRIM_RE = /^\s+|\s+$/g
        return function trim(string) {
            return string.replace(TRIM_RE, '')
        }
    }()

    function $c(staticClassName, conditionalClassNames) {
        var classNames = []
        if (typeof conditionalClassNames == 'undefined') {
            conditionalClassNames = staticClassName
        }
        else {
            classNames.push(staticClassName)
        }
        for (var className in conditionalClassNames) {
            if (!!conditionalClassNames[className]) {
                classNames.push(className)
            }
        }
        return classNames.join(' ')
    }

</script>