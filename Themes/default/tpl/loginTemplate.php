<div class="container">
    <div id="login-form"></div>
</div>


<script type="text/jsx">

    var Login = React.createClass({
        getInitialState: function() {
            return {
                submitted: null
            }
        }

        , render: function() {
            var submitted
            if (this.state.submitted !== null) {
                submitted = <div className="alert alert-success">
                    <p>LoginForm data:</p>
                    <pre><code>{JSON.stringify(this.state.submitted, null, '  ')}</code></pre>
                </div>
            }

            return <div>
                <div className="panel panel-default">
                    <div className="panel-body">
                        <h3 className="panel-title pull-left">Login</h3>
                        <LoginForm ref="loginForm"  onSubmit={this.handleSubmit}/>
                    </div>
                    <ExtraLinks ref="extraLinks"/>
                </div>
      {submitted}
            </div>
        }

        , handleChange: function(field, e) {
            var nextState = {}
            nextState[field] = e.target.checked
            this.setState(nextState)
        }

        , handleSubmit: function() {
            if (this.refs.loginForm.isValid()) {
                return true
            }
            this.setState({submitted: true})
            return false
        }
    })

    var ExtraLinks = React.createClass({
        render: function() {
            return <div className="extra-links">
                    {this.renderRegisterLink()}
                    </div>
        },

        renderRegisterLink : function () {
            return <a href="index.php?controller=register&action=index">You don't have an account yet ? Register</a>
        }
    });

    /**
     * A login form with certain optional fields.
     */
    var LoginForm = React.createClass({
        getDefaultProps: function() {
            return {
            }
        }

        , getInitialState: function() {
            return {errors: {}}
        }

        , isValid: function() {
            var fields = ['username', 'password']

            var errors = {}
            fields.forEach(function(field) {
                var value = trim(this.refs[field].getDOMNode().value)
                if (!value) {
                    errors[field] = 'This field is required'
                }
            }.bind(this))
            this.setState({errors: errors})

            var isValid = true
            for (var error in errors) {
                isValid = false
                break
            }
            return isValid
        }

        , getFormData: function() {
            var data = {
                username: this.refs.username.getDOMNode().value
                , password: this.refs.password.getDOMNode().value
            }

            return data
        }

        , render: function() {
            return <form className="form-horizontal"
                        action="index.php?controller=login&action=login" method="POST" onsubmit={this.props.onSubmit}>
                        {this.renderTextInput('username', 'User Name', 'username')}
                        {this.renderPasswordInput('password','Password', 'password')}
                        {this.renderSubmitInput('submit-login-form')}
            </form>
        }

        , renderTextInput: function(id, label, name) {
            return this.renderField(id, label, name,
                <input type="text" className="form-control" id={id} name={name} ref={id}/>
            )
        }

        , renderPasswordInput: function(id, label, name) {
            return this.renderField(id, label, name,
                <input type="password" className="form-control" name={name} id={id} ref={id}/>
            )
        }
        , renderSubmitInput: function(id) {
            return   <input type="submit" id={id} className="btn btn-primary btn-block"/>

        }

        , renderField: function(id, label, name, field) {
            return <div className={$c('form-group', {'has-error': id in this.state.errors})}>
                <label htmlFor={id} className="col-sm-4 control-label">{label}</label>
                <div className="col-sm-6">
                    {field}
                </div>
            </div>
        }
    })

    React.render(<Login company="Olivier Kessler"/>, document.getElementById('login-form'))

    // Utils

    var trim = function() {
        var TRIM_RE = /^\s+|\s+$/g
        return function trim(string) {
            return string.replace(TRIM_RE, '')
        }
    }()

    function $c(staticClassName, conditionalClassNames) {
        var classNames = []
        if (typeof conditionalClassNames == 'undefined') {
            conditionalClassNames = staticClassName
        }
        else {
            classNames.push(staticClassName)
        }
        for (var className in conditionalClassNames) {
            if (!!conditionalClassNames[className]) {
                classNames.push(className)
            }
        }
        return classNames.join(' ')
    }

</script>