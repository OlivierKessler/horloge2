<?php
namespace Core;

use \Controllers\Factories\ControllerFactory;

/**
 * Class Dispatcher
 *
 * Singleton Router
 */
class Dispatcher
{
    private static $instance;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Dispatcher();
        }

        return self::$instance;
    }

    public function dispatch($controllerName, $request = null)
    {
        $currentController = ControllerFactory::getObject($controllerName);
        $action = htmlspecialchars($request['action']);

        if ($action) {
            if (!$currentController->isValidAction($action)) {
                //die('Invalid Action in : '. __CLASS__);
            }
            else if(!$currentController->checkRights()) {
                //die('aa');
                Tools::redirect('login', 'index');
            }
            else {
                $method = $action.'Action';
                $currentController->$method();
            }
        }
        else {
            $currentController->indexAction();
        }
    }

    static function isValidRoute($controller, $action)
    {
        $currentController = ControllerFactory::getObject($controller);

        return array_key_exists($action, $currentController->_getActions());
    }
}