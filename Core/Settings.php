<?php
namespace Core;

class Settings
{

    public static $settings = null;

    public static function getSettings()
    {
        if(self::$settings == null) {
            self::$settings = yaml_parse_file(CORE_PATH . '/Settings/Settings.yaml');
        }
        return self::$settings;
    }

    public static function setSetting($name, $value)
    {
        yaml_emit_file(CORE_PATH.'/Settings/Settings.yaml', array($name => $value));
    }

    public static function getSetting($name)
    {
        self::getSettings();
        return self::$settings[$name];
    }
}