<?php
namespace Core\Hooks\Factories;

use \Core\Hooks as Hooks;
use \NSInterfaces\InterfaceFactorisator;

class HookFactory implements InterfaceFactorisator
{
    public static function getObject($type)
    {
        if ($type == "HookBody") {
            return $hookBody   = new Hooks\HookBody();
        }
        else if ($type == "HookFooter") {
            return $hookFooter = new Hooks\HookFooter();
        }
        else if ($type == "HookHeader") {
            return $hookHeader = new Hooks\HookHeader();
        }
        else if ($type == "HookBodyLogin") {
            return $hookHeader = new Hooks\HookBodyLogin();
        }
        else {
            return false;
        }
    }
}