<?php
namespace Core\Hooks;

use \Modules;

abstract class Hook
{
    public $name;
    public $disabled = 0;
    public $registeredModules;
    public $description;

    public function renderModules($action)
    {
        $htmlResultHookProcess = "";

        foreach ($this->registeredModules as $module){
            $namespace              = 'Modules\\'.$module['name'].'\\';
            $className              = $namespace . $module['name'];
            $currentModule          = new $className();
            $functionDisplay        = 'display'. $this->name.$action;
            if(method_exists($currentModule, $functionDisplay))
                $htmlResultHookProcess .= $currentModule->$functionDisplay();
        }

        return $htmlResultHookProcess;
    }
}