<?php
namespace Core\Hooks;

class HookFooter extends Hook
{
    public $name = "HookFooter";
    public $disabled = 0;
    public $registeredModules;
    public $description = "Hook of the body of the page";
}