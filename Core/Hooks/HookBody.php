<?php
namespace Core\Hooks;

class HookBody extends Hook
{
    public $name = "HookBody";
    public $disabled = 0;
    public $registeredModules = array(
        array(
            "name" => "MenuModule"
        ),
        array(
            "name" => "ClocksListModule"
        )
    );
    public $description = "Hook of the body of the page";
}