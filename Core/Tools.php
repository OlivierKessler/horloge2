<?php
namespace Core;

class Tools
{
    public static function getGetValue($index)
    {
        if (isset($_GET[$index]) && Validator::escape($_GET[$index])) {
            return Validator::escape($_GET[$index]);
        }

        return false;
    }

    public static function getPostValue($index)
    {
        if (isset($_POST[$index]) && Validator::escape($_POST[$index])) {
            return Validator::escape($_POST[$index]);
        }

        return false;
    }

    public static function getRequest()
    {
        return $_REQUEST;
    }

    public static function _crypt($input, $rounds = 10)
    {
        $crypt_options = array(
            'cost' => $rounds
        );
        return password_hash($input, PASSWORD_BCRYPT, $crypt_options);
    }

    public static function redirect($controller, $action)
    {
        header('Location: index.php?controller='.$controller.'&action='.$action);
    }
}