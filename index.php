<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

use Core\Dispatcher;
use Core\Tools;

define('CORE_PATH', dirname(__FILE__));

//require_once(CORE_PATH.'/Core/Settings.php');
require_once(CORE_PATH.'/Core/Autoloader.php');


$dispatcher = Dispatcher::getInstance();

if(Tools::getGetValue('controller') && Dispatcher::isValidRoute(Tools::getGetValue('controller'), Tools::getGetValue('action')))
{
    $dispatcher->dispatch(Tools::getGetValue('controller'), Tools::getRequest());
}
else if ($_SERVER['connected'] == true)
    $dispatcher->dispatch('home');
else
    $dispatcher->dispatch('login');