<?php
namespace Modules\ClocksListModule;

use Models\UserModel;
use Modules;

session_start();

class ClocksListModule extends Modules\Module
{
    public $moduleName    = "ClockList";
    public $moduleVersion = "0.1";

    public function __construct()
    {
    }

    /**
     * @return string
     *
     * display<hookname><action>
     */
    public function displayHookBodyindex()
    {
        $userModel = new UserModel($_SESSION['user_id']);

        $tpl_vars = array(
            "clocks" => $userModel->clocks
        );

        ob_start();
        require_once('tpl/body/list.php');
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

    public function displayHookBodyaddclock()
    {
        ob_start();
        require_once('tpl/addclock/add.php');
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }
}