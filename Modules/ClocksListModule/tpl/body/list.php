<div id="clocksListWrapper" class="container">
</div>
<div class="col l6 s12" id="buttonSwitch">
    <p class="grey-text text-lighten-4">SWITCH TO LISTVIEW</p>
</div>

<script src="Libraries/coolclock/coolclock.js" type="text/javascript"></script>
<script src="Libraries/coolclock/moreskins.js" type="text/javascript"></script>

<script type="text/jsx">

    var clocks = <?php echo json_encode($tpl_vars['clocks'])?>;

    var Clock = React.createClass({
        render: function() {
            var sliderId = "slider" + this.props.index;

            var style = {
              height : "100%",
                width : "100%"
            };

            return (
                <div id={sliderId} className="col s6 m3 l3 slider">
                    <div  data-slidr="one">
                        <canvas  className={this.props.class}>
                            {/*this.props.clock.city*/}

                        </canvas>
                        <div className="city-label">{this.props.clock.city}</div>
                    </div>
                    <form method="post" action="index.php?controller=home&action=processremove" data-slidr="two"
                        className="options-clock grey darken-3" style={style}>
                        <input type="hidden" name="clock-id" value={this.props.clock._id}/>
                        <div className="button-container">
                            <input type="submit" className="btn" value="Delete"/>
                        </div>
                    </form>
                </div>
            );
        }
    });

    var ClockModeList = React.createClass({
        render: function() {
            return (
                <div className={this.props.class}>
                    <div className = "info-wrapper">
                    </div>
                    <div className = "hour-wrapper">
                    </div>
                </div>
            );
        }
    });


    var ProperListRender = React.createClass({
        render: function() {

            if (this.props.mode == "grid") {
                return (
                    <div className="row">
                      {this.props.list.map(function (listValue, i) {
                          var classes = "CoolClock:Tes2:::"+ listValue.rawOffset/3600 +" clockBlock card-panel lighten-1 z-depth-1 " + listValue.color;
                          return <Clock clock={listValue} key={i} index={i}
                              class={classes} />;
                      })}
                    </div>
                )
            } else if (this.props.mode == "list") {
                return (
                    <div className="row">
                      {this.props.list.map(function (listValue, i) {
                          var classes = "CoolClock:Tes2 clockBlock card-panel lighten-1 z-depth-1 " + listValue.color;
                          return <ClockModeList clock={listValue} key={i}
                              class={classes} />;
                      })}
                    </div>
                )
            }
        }
    });

    $(document).ready(function() {

        function renderGridView()
        {
            React.render(<ProperListRender list={clocks} mode="grid"/>, document.getElementById('clocksListWrapper'));
            CoolClock.findAndCreateClocks();
            initSliders();
        }

        function renderListView()
        {
            React.render(<ProperListRender list={clocks} mode="list" />, document.getElementById('clocksListWrapper'));
        }

        function initSliders() {
            $('.slider').each(function () {
                var slider = slidr.create(this.id, {
                    after: function (e) {
                        console.log('in: ' + e.in.slidr);
                    },
                    before: function (e) {
                        console.log('out: ' + e.out.slidr);
                    },
                    breadcrumbs: false,
                    controls: 'false',
                    direction: 'horizontal',
                    fade: false,
                    keyboard: true,
                    overflow: true,
                    theme: '#222',
                    timing: {'cube': '0.5s ease-in'},
                    touch: true,
                    transition: 'cube'
                }).start();

                $(this).on({
                    mouseenter: function () {
                        event.preventDefault();
                        event.stopPropagation();
                        event.stopImmediatePropagation();
                        slider.slide("right");
                    },
                    mouseleave: function () {
                        event.preventDefault();
                        event.stopPropagation();
                        event.stopImmediatePropagation();
                        slider.slide("left");
                    }
                });
            });
        }


        $('#buttonSwitch').on('click', function(){
            if($(this).hasClass('grid-switch')) {
                $(this).children('p').text('SWITCH TO LISTVIEW');
                $(this).removeClass('grid-switch');
                $(this).addClass('list-switch');
                renderGridView();
            } else {
                $(this).children('p').text('SWITCH TO GRIDVIEW');
                $(this).removeClass('list-switch');
                $(this).addClass('grid-switch');
                renderListView();
            }
        });

        $('.clockBlock').hover(
            function() {
                $(this).removeClass('z-depth-1');
                $( this ).addClass('z-depth-2');
            },
            function() {
                $(this).removeClass('z-depth-2');
                $( this ).addClass('z-depth-1');
            }
        );


        renderGridView();
    });
</script>

