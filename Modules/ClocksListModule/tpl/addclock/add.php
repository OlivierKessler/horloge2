<div class="container">
    <div class="col-sm-6">
        <input type="text" id="search-field"/>
    </div>
    <div id="add-clock-form-wrapper">
    </div>
</div>

<script type="text/jsx">
        var ListItem = React.createClass({
            render: function() {

                var colors = [
                    "red",
                    "pink",
                    "purple",
                    "deep-purple",
                    "indigo",
                    "blue",
                    "light-blue",
                    "cyan",
                    "teal",
                    "green",
                    "lime"
                ];

                /**
                 * TODO : I really need to make this treatments at the submit
                 *
                 */

                var country;
                var city;

                //Get country
                for (var i = 0; i < this.props.item.address_components.length; i++) {
                    if (this.props.item.address_components[i].types[0] == "country") {
                        country = this.props.item.address_components[i].long_name;
                    }
                    if (this.props.item.address_components[i].types.length == 2) {
                        if (this.props.item.address_components[i].types[0] == "political") {
                            country = this.props.item.address_components[i].long_name;
                        }
                    }
                }

                //Get city
                for (var i = 0; i < this.props.item.address_components.length; i++) {
                    for(var j = 0; this.props.item.address_components[i].types; j++){
                        if(this.props.item.address_components[i].types[j] =='locality') {
                            city = this.props.item.address_components[i].long_name;
                            break;
                        }
                    }
                    if(city !== undefined)
                        break;
                }

                var inputValue = city + "," + country;



                return (
                    <form className="select-city" action="index.php?controller=home&action=processadd" method="post" data-key={this.props.key}>
                        <div className="waves-effect waves-light btn btn-modal">
                            {this.props.item.formatted_address}
                            <input name="city" type="hidden" value={inputValue} readonly/>
                            <input name="lat" type="hidden" value={this.props.item.geometry.location.lat}/>
                            <input name="lng" type="hidden" value={this.props.item.geometry.location.lng}/>
                            <input name="offset" type="hidden" value={this.props.item.gmtOffset}/>
                            <input name="color" id="color-input" type="hidden" value="Yellow" />
                        </div>
                        <div id={this.props.classModal} className="modal">
                            <div className="modal-content">
                                <h4 className="center-align">Add a Clock</h4>
                                <h5>Pick a color :</h5>
                                <ColorPicker colors={colors}/>
                            </div>
                            <div className="modal-footer">
                                <a href="#!" className=" modal-action modal-close waves-effect waves-green btn-flat">Yes</a>
                            </div>
                        </div>
                    </form>
                );
            }
        });

        var ColorPicker = React.createClass({
           render : function () {
               return (
                   <div className="row">
                       <ul>
                          {this.props.colors.map(function(listValue, i){
                              classes = listValue + " " + "color-picker";
                              return <div className={classes} data-color={listValue}></div>;
                          })}
                       </ul>
                   </div>
               )
           }
        });

        var ListAddClock = React.createClass({
            render: function () {
                var items = this.getList(this.props.city);

                return (
                    <div className="row">
                        <ul>
                          {items.results.map(function (listValue, i) {
                              var classModal = "modal" + i;
                              return <ListItem  key={i} classModal={classModal} item={listValue} classIcon="mdi-content-language" />;
                          })}
                        </ul>
                    </div>
                )
            },
            getList: function (city) {

                var response = $.ajax({
                    url: 'https://maps.googleapis.com/maps/api/geocode/json',
                    type: 'GET',
                    dataType: 'json',
                    async: false,
                    data: {
                        'key': 'AIzaSyCCqia1dGRmWDO37PN9R6h3uDIqIXAvFYE',
                        'address': city
                    },
                    success: function (result) {
                        return result;
                    }
                }).responseJSON;


                for(var i = 0; i < response.results.length; i++) {
                    var location = response.results[i].geometry.location.lat + "," + response.results[i].geometry.location.lng;

                    response.results[i].gmtOffset = $.ajax({
                        url: 'https://maps.googleapis.com/maps/api/timezone/json',
                        type: 'GET',
                        data: {
                            'key': 'AIzaSyCCqia1dGRmWDO37PN9R6h3uDIqIXAvFYE',
                            'location': location,
                            'timestamp' : "0"
                        },
                        dataType: 'json',
                        async: false,
                        success: function (result, statut) {
                            return result;
                        }
                    }).responseJSON.rawOffset;
                }

                return response;
            }
        });




    $(document).ready(function() {
        /*$('#search-field').on('keyup change', function() {
            var city = $(this).val();
            React.render(<ListAddClock city={city}/>, document.getElementById('add-clock-form-wrapper'));
        });*/

        $('#search-field').on('change', function() {
         var city = $(this).val();
         React.render(<ListAddClock city={city} />, document.getElementById('add-clock-form-wrapper'));
         });

        setInterval(function(){
            $('.select-city').on('click', function(){
                $(this).children('.modal').openModal();
            });

            $('.modal-close').on('click', function(){
                $('#color-input').val($('.color-picker.selected').data("color"));
                $(this).parents('.select-city').submit();
            });


            $('.color-picker').on('click', function(event)
            {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();

                $('.color-picker').removeClass('selected');
                $(this).addClass('selected');
            })
        }, 1000);
    });
</script>