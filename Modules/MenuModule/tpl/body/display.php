<nav>
    <div class="nav-wrapper">
        <div class="brand-logo center"><a href="index.php" ><img src="Themes/default/img/logo.png" alt="Logo"/></a></div>
        <ul class="right hide-on-med-and-down">
            <!-- <li><a class="grey darken-2" href="index.php?controller=parameters">Parameters</a></li>-->
             <li><a class="grey darken-2" href="index.php?controller=home&action=addclock">Add Clock</a></li>
             <li><a class="grey darken-2" href="index.php?controller=login&action=disconnect">Disconnect</a></li>
         </ul>
         <ul id="slide-out" class="side-nav">
             <li class="logo"><a id="logo-container" href="http://materializecss.com/" class="brand-logo"></a></li>
             <li><a href="index.php?controller=home">Home</a></li>
             <li><a href="index.php?controller=home&action=addclock">Add Clock</a></li>
             <!--<li><a href="index.php?controller=parameters"></i>Parameters</a></li> -->
            <li><a href="index.php?controller=login&action=disconnect">Disconnect</a></li>
        </ul>
        <a href="#" data-activates="slide-out" class="button-collapse menuMobileExpander"><i class="mdi-content-add"></i></a>
        <a href="javascript:window.location.reload()" id="refreshButton" class="button-collapse"><i class="mdi-navigation-refresh"></i></a>
    </div>
</nav>

<script>
$(".menuMobileExpander").sideNav({
        menuWidth: 240, // Default is 240
        edge: 'left', // Choose the horizontal origin
        closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
    }
);
</script>

