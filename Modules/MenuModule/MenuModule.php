<?php
namespace Modules\MenuModule;

use Modules;

class MenuModule extends Modules\Module
{
    public $moduleName    = "MenuModule";
    public $moduleVersion = "0.1";

    function __construct()
    {
    }

    public function displayHookBodyindex()
    {
        ob_start();
        include 'tpl/body/display.php';
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

    public function displayHookBodyaddclock()
    {
        ob_start();

        include 'tpl/body/display.php';
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }
}