<?php
namespace Modules;

abstract class Module
{
    public $moduleName;
    public $moduleVersion;

    abstract public function __construct();

    protected function addOnHook($hookName)
    {
        $currentHook = HookFactory::getObject($hookName);
        $currentHook->addModule($this->name);
    }

    protected function removeFromHook($hookName)
    {
        $currentHook = HookFactory::getObject($hookName);
        $currentHook->removeModule($this->name);
    }
}