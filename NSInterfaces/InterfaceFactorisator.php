<?php
namespace NSInterfaces;

interface InterfaceFactorisator
{
     static function getObject($type);
}