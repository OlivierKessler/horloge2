<?php
namespace Models;

use Core\Tools;

class UserModel extends Model
{
    public $collection = null;
    public $id         = "";
    public $name       = "";
    public $username   = null;
    public $clocks     = array();

    /**
     * Set the MongoDB and MongoCollection objects
     * @param String $id  If user Id provided, initialize complete Model
     */
    public function __construct($id = null)
    {
        self::setDB();
        $this->collection = self::$DB->clocks_project;

        if(isset($id)) {
            if($user = $this->checkIfIdUserExists($id)) {
                $this->id       = $user['_id'];
                $this->name     = $user['name'];
                $this->username = $user['username'];
                $this->clocks   = $user['clocks'];
            }
            else {
                die('User doesn\'t exist');
            }
        }
    }


    /**
     * @param String $username  Plain Text Username
     * @param String $password  Plain Text Password
     *
     * @return boolean          The authentification has succeded or not
     */
    public function login($username, $password)
    {
        $userByUsername = $this->getUserByUsername($username);

        if(!isset($userByUsername) || empty($userByUsername)) {
            echo 'User doesn\'t exist !';
            return false;
        }

        if (password_verify($password, $userByUsername['password'])) {
            echo 'Valid password !';
            return $userByUsername['_id'];
        } else {
            echo 'Invalid password !';
            return false;
        }
    }



    /**
     * *****************************************  Clocks CRUD *************************************
     */

    public function getAllClocks()
    {
        $query = array("_id" => $this->id);
        $cursor = $this->collection->find($query);

        if(isset($cursor['0'])) {
            return $this->clocks = $cursor['0']['clocks'];
        }

        return "";
    }

    public function addClock($clockDocument)
    {
        $this->collection->update(
            array('_id'=> $_SESSION['user_id']),
            array('$push' => array('clocks' => $clockDocument))
        );
    }

    public function removeAllClocks()
    {
        $this->collection->update(
            array('_id'=> $_SESSION['user_id']),
            array('$unset' => array('clocks' => array('$regex' => 'i')))
        );
    }

    public function removeClock($clockId)
    {
        return $this->collection->update(
            array('_id'=> $_SESSION['user_id']),
            array('$pull' => array('clocks' => array('_id' => $clockId)))
        );
    }

    public function updateClock($clockDocument)
    {
    }

    public function getClock($clockDocument)
    {
    }

    /**
     * *****************************************  Users CRUD *************************************
     */


    public function removeUser($id)
    {
        $this->collection->remove(array('_id' => new MongoId($id)), true);
    }

    /**
     * Warning with this
     */
    public function removeAllUsers()
    {
        $this->collection->remove();
    }

    public function updateUser($id, $datas)
    {
        //TODO : Implement updates
    }

    public function demoUser()
    {
        $document = array(
            "name"     => "Olivier Kessler",
            "username" => "ExpressAlive2",
            "password" => Tools::_crypt("testpassword"),
            "clocks"   => array(
                array(
                    "_id"      => uniqid(),
                    "city"     => "Londres",
                    "location" => "51.5073509, -0.1277583",
                    "color"    => "Yellow",
                    "position" => 0,
                    "style"    => "classic"
                ),
                array(
                    "_id"      => uniqid(),
                    "city"     => "Londres",
                    "location" => "51.5073509, -0.1277583",
                    "color"    => "Yellow",
                    "position" => 0,
                    "style"    => "classic"
                )
            )
        );


        $this->collection->insert($document);
    }

    public function addUser($login, $password){

        if($login && $password) {
            $document = array(
                "name" => $login,
                "username" => $login,
                "password" => Tools::_crypt($password),
                "clocks" => array()
            );

            return $this->collection->insert($document);
        }
        else{
            return false;
        }
    }

    public function getUserById($id)
    {
        $query = array("_id" => $id);
        $cursor = $this->collection->find($query);

        if (isset($cursor['0'])) {
            return $cursor['0'];
        }
        return false;
    }

    public function getUserByUsername($username)
    {
        $query = array('username' => $username);
        $cursor = $this->collection->find($query);

        foreach ($cursor as $document) {
            $documentUser[] = $document;
        }

        if (isset($documentUser['0'])) {
            return $documentUser['0'];
        }
        return false;
    }

    public function checkIfIdUserExists($id)
    {
        $query = array("_id" => $id);
        $cursor = $this->collection->find($query);

        foreach ($cursor as $document) {
            $documentUser[] = $document;
        }

        if (isset($documentUser['0'])) {
            return $documentUser['0'];
        }
        return false;
    }
}