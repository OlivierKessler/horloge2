<?php
namespace Models;

use Core\Settings;
use MongoClient;

abstract class Model
{
    protected static $DB          = null;
    protected static $MongoClient = null;

    private static function setMongoClient()
    {
        if (self::$MongoClient == null) {
            self::$MongoClient = new MongoClient();
        }
    }

    public static function setDB()
    {
        self::setMongoClient();

        if (self::$DB == null){
            $databaseName = Settings::getSetting('database_name');
            if ($databaseName != null) {
                self::$DB = self::$MongoClient->$databaseName;
            }
            else {
                die('The Database name in the settings.yaml is wrong');
            }
        }
    }
}