<?php

namespace Controllers;

use Models\UserModel;
use Core\Tools;

class HomeController extends Controller
{
    protected $_actions_hooks = array (
        "index" => array(
            "hooks" => array(
                "HookHeader",
                "HookBody",
                "HookFooter"
            )
        ),
        "addclock" => array(
            "hooks" => array(
                "HookHeader",
                "HookBody",
                "HookFooter"
            )
        ),
        "processadd" => array(
            "hooks" => array()
        ),
        "processremove" => array(
            "hooks" => array()
        )
    );

    public function __construct(){}

    /**
     ************INDEX ACTION****************
     */
    public function postProcessIndex()
    {
        return "";
    }

    public function renderViewIndex($datas = null)
    {
        $this->_hooksResult = $this->callHooks('index');
        $this->callTemplates();
    }

    public function indexAction()
    {
        $extraDatas = $this->postProcessIndex();
        $this->renderViewIndex($extraDatas);
    }

    /**
     * Add Clock Action
     */

    private function postProcessAddClock()
    {
        return "";
    }

    private function renderViewAddClock()
    {
        $this->_hooksResult = $this->callHooks('addclock');
        $this->callTemplates();
    }

    public function addclockAction()
    {
        $datas = $this->postProcessAddClock();
        $this->renderViewAddClock($datas);
    }

    /**
     * Process add action
     */

    public function processaddAction()
    {
        $this->postProcessProcessAdd();
        Tools::redirect('home', 'index');
    }

    private function postProcessProcessAdd()
    {
        if(Tools::getPostValue("lat") && Tools::getPostValue("lng") &&
            Tools::getPostValue("city") && Tools::getPostValue("color")
        && Tools::getPostValue("offset")){



            $userModel = new UserModel($_SESSION['user_id']);

            $document = array(
                "_id"      => uniqid(),
                "city"     => Tools::getPostValue("city"),
                "location" => Tools::getPostValue("lat"). ',' . Tools::getPostValue("lng"),
                "color"    => Tools::getPostValue("color"),
                "position" => 0,
                "style"    => "classic",
                "rawOffset" => Tools::getPostValue("offset")
            );

            $userModel->addClock($document);
        }
    }

    /**
     * Process remove action
     */
    public function processremoveAction()
    {
        $this->postProcessProcessRemove();
        Tools::redirect('home', 'index');
    }

    private function postProcessProcessRemove()
    {
        if(Tools::getPostValue("clock-id")){
            $clockId = Tools::getPostValue("clock-id");
            $userModel = new UserModel($_SESSION['user_id']);
            $userModel->removeClock($clockId);
        }
    }
}