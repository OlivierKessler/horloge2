<?php

namespace Controllers;

class PreferencesController extends Controller
{
    protected $_actions_hooks = array (
        "index"
    );

    public function __construct()
    {
    }

    /**
     *        ***********INDEX ACTION****************
     */
    public function postProcessIndex()
    {
        return "";
    }

    public function renderViewIndex($datas = null)
    {
        $this->hooksResult = $this->callHooks('index');
        $this->callTemplates();
    }

    public function indexAction()
    {
        $extraDatas = $this->postProcessIndex();
        $this->renderViewIndex($extraDatas);
    }
}