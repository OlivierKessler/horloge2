<?php

namespace Controllers;

use Core\Tools;
use Models\UserModel;

class LoginController extends Controller
{
    const FAILED_LOGIN_STATUS = "AUTH_FAILED";

    protected $_loginNeeded = false;

    protected $_actions_hooks = array (
        "index" => array(
            "hooks" => array()
        ),
        "login" => array(
            "hooks" => array()
        ),
        "error" => array(
            "hooks" => array()
        ),
        "disconnect" => array(
            "hooks" => array()
        )
    );

    public function __construct(){}

    /**
     * --------------- INDEX ACTION ----------------------------
     */

    public function postProcessIndex()
    {
        //$userModel = new UserModel();
        //$userModel->demoUser();

        return "";
    }

    public function renderViewIndex($datas = null)
    {
        $this->callTemplates($datas);
    }

    public function callTemplates($template_datas){
        require_once 'Themes/default/tpl/headerTemplate.php';
        require_once 'Themes/default/tpl/bodyTemplate.php';
        require_once 'Themes/default/tpl/loginTemplate.php';
        require_once 'Themes/default/tpl/footerTemplate.php';
    }

    public function indexAction()
    {
        if(isset($_SESSION['user_id']))
            Tools::redirect('home', 'index');
        $extraDatas = $this->postProcessIndex();
        $this->renderViewIndex($extraDatas);
    }

    /**
     * --------------- LOGIN ACTION----------------------------------
     */

    public function postProcessLogin()
    {
        $username = Tools::getPostValue('username');
        $password = Tools::getPostValue('password');

        $userModel = new UserModel();

        if(!$username || !$password)
            return false;
        else {
            return $userModel->login($username, $password);
        }
    }

    public function loginAction()
    {
        $idUser = $this->postProcessLogin();

        if($idUser) {
            $_SESSION['user_id'] = $idUser;
            Tools::redirect("home", "index");
        }
        else
            Tools::redirect("login", "error");
    }

    /**
     * ******ERROR ACTION
     */

    public function errorAction()
    {
        $this->renderViewIndex(array(
                "error_code" => self::FAILED_LOGIN_STATUS
            )
        );
    }


    /**
     * ******DISCONNECT ACTION
     */

    public function disconnectAction()
    {
        session_destroy();
        Tools::redirect('login', 'index');
    }
}