<?php
namespace Controllers\Factories;

use \Controllers as Controllers;
use \NSInterfaces\InterfaceFactorisator;

class ControllerFactory implements InterfaceFactorisator
{
    public static function getObject($type)
    {
        if ($type == "home") {
            return  new Controllers\HomeController();
        }
        else if ($type == "preferences") {
            return  new Controllers\PreferencesController();
        }
        else if ($type == "login") {
            return new Controllers\LoginController();
        }
        else if ($type == "register") {
            return new Controllers\RegisterController();
        }
        else {
            return new Controllers\ErrorController();
        }
    }
}