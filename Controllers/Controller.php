<?php
namespace Controllers;

use Core\Hooks\Factories\HookFactory;
use Core\Validator;
use Core\Tools;

abstract class Controller
{
    protected $_controllerName;
    protected $_arrayModules;
    protected $_actions_hooks = array();
    protected $_hooksResult;
    protected $_loginNeeded     = true;


    public function addJS($path)
    {
        if (Validator::isValidJSPath($path)){
            $this->_hooksResult += Tools::generateJSInclusion($path);
            return true;
        }
        else {
            return false;
        }
    }

    public function isValidAction($action){
        return method_exists($this, $action.'Action');
    }

    public function checkRights(){
        if($this->_loginNeeded && !isset($_SESSION['user_id']))
            return false;
        return true;
    }

    /**
     *    The index action is needed for every Controller
     */
    abstract public function indexAction();

    abstract public function postProcessIndex();

    abstract public function renderViewIndex($datas = null);



    public function callHooks($action)
    {
        $resultHookProcessing = "";

        foreach ($this->_actions_hooks[$action]['hooks'] as $hookName) {
            $resultHookProcessing[$hookName]  = "";
            $currentHook                      = HookFactory::getObject($hookName);
            $resultHookProcessing[$hookName] .= $currentHook->renderModules($action);
        }
        return $resultHookProcessing;
    }

    protected function callTemplates($controller_datas = null)
    {
        $controller = get_called_class();
        require_once 'Themes/default/tpl/headerTemplate.php';
        require_once 'Themes/default/tpl/bodyTemplate.php';
        require_once 'Themes/default/tpl/footerTemplate.php';
    }

    public function _getActions()
    {
        return $this->_actions_hooks;
    }
}