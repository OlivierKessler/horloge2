<?php

namespace Controllers;

use Core\Tools;
use Models\UserModel;

class RegisterController extends Controller
{

    protected $_loginNeeded = false;

    protected $_actions_hooks = array (
        "index" => array(
            "hooks" => array()
        ),
        "register" => array(
            "hooks" => array()
        )
    );

    public function __construct(){}

    /**
     * --------------- INDEX ACTION ----------------------------
     */

    public function postProcessIndex()
    {
        return "";
    }

    public function renderViewIndex($datas = null)
    {
        $this->hooksResult = $this->callHooks('index');
        $this->callTemplates();
    }

    public function callTemplates(){
        require_once 'Themes/default/tpl/headerTemplate.php';
        require_once 'Themes/default/tpl/bodyTemplate.php';
        require_once 'Themes/default/tpl/registerTemplate.php';
        require_once 'Themes/default/tpl/footerTemplate.php';
    }

    public function indexAction()
    {
        $extraDatas = $this->postProcessIndex();
        $this->renderViewIndex($extraDatas);
    }

    /**
     * --------------- REGISTER ACTION----------------------------------
     */

    public function postProcessRegister()
    {
        $userModel = new UserModel();
        return $userModel->addUser(Tools::getPostValue("username"), Tools::getPostValue("password"));
    }

    public function renderViewRegister($datas = null)
    {
        $this->hooksResult = $this->callHooks('register');
        $this->callTemplates();
    }

    public function registerAction()
    {
        $resultRegister = $this->postProcessRegister();

        if($resultRegister) {
            Tools::redirect("login", "index");
        }
        else {
            Tools::redirect("register", "error");
        }
    }

    public function errorAction(){
        $this->renderViewIndex(array("error_code" => self::FAILED_REGISTER_STATUS));
    }
}